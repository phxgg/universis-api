### Students Services Summary

The following document contains information about the core services for getting and editing data related to a student.
 
 #### Send message to a student
 
 Sending a message to a student related or not with a student request action
 
     POST /api/Students/{id}/messages/send
     Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
     Content-Type: multipart/form-data
     Accept-Language: el
     
 where authorization header contains user access token received from OAuth2 authentication server,
 content-type defines a multipart/form-data request and accept-language header is the preferred locale 
 for this request.
 
 Form data can contain a field called "file" which is the file (*.csv or *.xlsx) that is going to be uploaded.
       
       POST /api/Students/9999/messages/send HTTP/1.1
       Host: localhost:5001
       Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
       Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW
       cache-control: no-cache
       Postman-Token: 34a0b0cc-dd53-425c-9a86-a7dbefed0161
       
       Content-Disposition: form-data; name="attachment"; filename="C:\Projects\universis\universis-teachers\src\assets\img\universis_logo_128_color.png
       
       
       
       Content-Disposition: form-data; name="body"
       
       Welcome
       
       Content-Disposition: form-data; name="action"
       
       31
       ------WebKitFormBoundary7MA4YWxkTrZu0gW--
 
 The response of this operation is a StudentMessage object:
 
     {
         "body": "Welcome",
         "action": 31,
         "student": 9999,
         "recipient": 5678,
         "sender": 1234,
         "owner": 1234,
         "identifier": "9282FB86-EEF0-4271-AC42-846D7DFC9D6C",
         "dateCreated": "2019-04-18T08:26:58.484Z",
         "dateModified": "2019-04-18T08:26:58.530Z",
         "createdBy": 1234,
         "modifiedBy": 1234,
         "id": 23,
         "attachments": [
             {
                 "contentType": "image/png",
                 "owner": 5678,
                 "version": 1,
                 "published": false,
                 "alternateName": "oBxRQQbadN7R",
                 "url": "\\api\\content\\private\\oBxRQQbadN7R",
                 "additionalType": "Attachment",
                 "dateCreated": "2019-04-18T08:26:58.838Z",
                 "dateModified": "2019-04-18T08:26:58.884Z",
                 "createdBy": 1234,
                 "modifiedBy": 1234,
                 "id": 1073
             }
         ]
     }
 
 #### Get student course exam statistics
 
 Returns an aggregated result which represents course exam grade statistics.
 
      GET /api/students/me/exams/{courseExam}/statistics
      Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
      Content-Type: application/json
      Accept-Language: el

The response of this operation is an array of items. Each item contains the number of students that have been graded with the specified`examGrade`. 
`isPassed` flag indicates whether a grade is passed or not.


    [
        {
            "examGrade": 0.25,
            "total": 1,
            "isPassed": 0
        },
        ...
        {
            "examGrade": 0.87,
            "total": 5,
            "isPassed": 1
        },
        {
            "examGrade": 0.9,
            "total": 2,
            "isPassed": 1
        }
    ]

 #### Get available graduation events
 
 Returns an aggregated result which represents available graduation event that student can apply.
 
      GET /api/students/me/availableGraduationEvents
      Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
      Content-Type: application/json
      Accept-Language: el

The response of this operation is an array of open graduationEvents that refers to student study program . 

    [
        {
                  "id": 1,
                  "organizer": "422",
                  "validFrom": "2020-03-31T00:00:00.000Z",
                  "validThrough": "2020-04-30T00:00:00.000Z",
                  ...
        }
    ]

 #### Send student graduation request
 
 Send a student graduation request
 
     POST /api/graduationRequestActions
     Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
     Content-Type: application/json
     Accept-Language: el
     
 where authorization header contains user access token received from OAuth2 authentication server,
  
 POST data
 
     {
         "graduationEvent": {
             "id": 12
         },
         "name": "Υποβολή αίτησης ορκωμοσίας"
     }
       
        
 
 The response of this operation is a GraduationRequestAction object:
 
     {
                 "name": "Student graduation request"
                 "description": null,
                 "additionalType": "GraduationRequestAction",
                 "graduationEvent": 1
                 ...
                
     }

#### Get student graduation requests
 
 Returns an aggregated result which represents student graduation requests
 
      GET /api/students/me/graduationRequests
      Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
      Content-Type: application/json
      Accept-Language: el

The response of this operation is an array of open GraduationRequestAction. AttachmentTypes attribute represents allowed attachment types for student request

    [
        "description": null,
                   "additionalType": "GraduationRequestAction",
                   "graduationEvent": 1,
                   "actionStatus": {
                       "additionalType": "ActionStatusType",
                       "alternateName": "PotentialActionStatus",
                       "name": "Potential",
                       "identifier": null,
                       "id": 1,
                       "description": "A description of an action that is supported.",
                       "image": null,
                       "url": "http://schema.org/PotentialActionStatus",
                       "dateCreated": "2018-10-12T06:41:00.411Z",
                       "dateModified": "2018-10-12T06:41:00.411Z",
                       "createdBy": 20054555,
                       "modifiedBy": 20054555
                   },
                   ...
                   "attachmentTypes": [
                                   {
                                       "target": "GraduationEvent",
                                       "object": "F7EF0A77-1445-467B-BD49-609D2D48DEBD",
                                       "id": 6,
                                       "attachmentType": 1,
                                       "numberOfAttachments": 1,
                                       "sortIndex": null,
                                       "additionalType": null,
                                       "alternateName": null,
                                       "description": null,
                                       "image": null,
                                       "name": null,
                                       "url": null,
                                       "dateCreated": "2020-04-06T07:54:46.547Z",
                                       "dateModified": "2020-04-06T07:54:47.151Z",
                                       "createdBy": 20000506,
                                       "modifiedBy": 20000506
                                   },
                                   {
                                       "target": "GraduationEvent",
                                       "object": "F7EF0A77-1445-467B-BD49-609D2D48DEBD",
                                       "id": 7,
                                       "attachmentType": 2,
                                       "numberOfAttachments": 1,
                                       "sortIndex": null,
                                       "additionalType": null,
                                       "alternateName": null,
                                       "description": null,
                                       "image": null,
                                       "name": null,
                                       "url": null,
                                       "dateCreated": "2020-04-06T07:54:53.588Z",
                                       "dateModified": "2020-04-06T07:54:53.801Z",
                                       "createdBy": 20000506,
                                       "modifiedBy": 20000506
                                   }
                               ]
                               ....
    ]

#### Add attachment to a student request action
  
 Adding an attachment to a student request action
 
     POST /api/Students/me/requests/{request}/attachments/add
     Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
     Content-Type: multipart/form-data
     Accept-Language: el
     
 where authorization header contains user access token received from OAuth2 authentication server,
 content-type defines a multipart/form-data request and accept-language header is the preferred locale 
 for this request.
 
 Form data can contain a field called "file" which is the file (*.csv or *.xlsx) that is going to be uploaded.
 and body should contain file(attachmentType) which refers to allowed attachmentType object 
       
       POST /api/students/me/requests/2226/attachments/add HTTP/1.1
       Host: localhost:5001
       Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
       Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW
       cache-control: no-cache
       Postman-Token: 34a0b0cc-dd53-425c-9a86-a7dbefed0161
       
       Content-Disposition: form-data; name="file"; filename="C:\Projects\universis\universis-teachers\src\assets\img\universis_logo_128_color.png
       
       Content-Disposition: form-data; name="body"
       
       file[attachmentType]=1
       
       ------WebKitFormBoundary7MA4YWxkTrZu0gW--
 
 The response of this operation is a Attachment object:
 
       {
           "attachmentType": "1",
           "contentType": "text/csv",
           "version": 1,
           "published": false,
           "alternateName": "aD2ezVAcqhcU",
           "url": "/api/content/private/aD2ezVAcqhcU",
           "additionalType": "Attachment",
           "owner": 20000081,
           "dateCreated": "2020-04-06T08:34:51.254Z",
           "dateModified": "2020-04-06T08:34:51.256Z",
           "createdBy": 20000081,
           "modifiedBy": 20000081,
           "id": 1563
       }

#### Remove attachment from a student request action
  
 
       POST /api/students/me/requests/{request}/attachments/{attachment}/remove
          Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
          Content-Type: application/json
          Accept-Language: el
          
  where authorization header contains user access token received from OAuth2 authentication server,
   
  The response of this operation is a GraduationRequestAction object:
  
      {
          "id": 1563,
          "additionalType": "Attachment",
          "alternateName": "aD2ezVAcqhcU",
          "contentType": "text/csv",
          "datePublished": null,
          "published": false,
          "keywords": null,
          "thumbnail": null,
          "version": "1",
          "attachmentType": 1,
          "owner": 20000081,
          "description": null,
          "image": null,
          "name": "logo.png",
          "url": "/api/content/private/aD2ezVAcqhcU",
          
      } 
