import {RequestHandler} from 'express';
const ics = require('ics');

const icsContentType = 'text/calendar';

/**
 * Extends Request to send xls data
 * @returns {RequestHandler}
 */
function icsParser() {
    // noinspection JSValidateTypes
    return function (req, res, next) {
        /**
         * @method
         * @name Response~ics
         * @this Response
         * @param {*} data
         * @returns Promise
         */
        res.ics = function (data) {
            /**
             * @type {Response}
             */
            let self = this;
            return new Promise(function (resolve, reject) {
                if (Array.isArray(data)) {
                    // set status
                    let icsEvents = [];
                    data.forEach(event => {
                        const parsedStartDate = new Date(event.startDate);
                        const parsedEndDate = new Date(event.endDate);
                        const parsedEvent = {
                            productId: 'universis/ics',
                            title: event.name,
                            start: [parsedStartDate.getFullYear(), parsedStartDate.getMonth(), parsedStartDate.getDate(), parsedStartDate.getHours(), parsedStartDate.getMinutes(), parsedStartDate.getSeconds()],
                            end: [parsedEndDate.getFullYear(), parsedEndDate.getMonth(), parsedEndDate.getDate(), parsedEndDate.getHours(), parsedEndDate.getMinutes(), parsedEndDate.getSeconds()],
                            description: event.description,
                            location: event.location,
                            url: event.url,
                            organizer: event.hasOwnProperty('organizer') && typeof event.organizer == 'object' && event.organizer.hasOwnProperty('name') && event.organizer.name && event.organizer.hasOwnProperty('email') && event.organizer.email ?
                                {
                                    name: event.organizer.name || event.organizer.alternateName,
                                    email: event.organizer.email
                                } : null
                        }
                        icsEvents.push(parsedEvent);
                    });
                    let parseIcs;
                    ics.createEvents(icsEvents, (err, value) => {
                        if (err) {
                            return reject(err);
                        }
                        parseIcs = value;
                    });
                    self.status(200)
                        .set('content-type', icsContentType)
                        .send(parseIcs);
                    return resolve();
                }
                // unprocessable entity
                self.status(422);
                // and finally return
                return reject();
            });
        };
        return next();
    }
}

module.exports.icsParser = icsParser;
module.exports.icsContentType = icsContentType;

