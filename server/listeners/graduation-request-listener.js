/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */

import _ from "lodash";
import {GenericDataConflictError} from "../errors";
import ActionStatusType from "../models/action-status-type-model";
import {DataError, DataNotFoundError} from "@themost/common";

export function beforeSave(event, callback) {
    return GraduationRequestEventListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

export function afterExecute(event, callback) {
    return GraduationRequestEventListener.afterExecuteAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return GraduationRequestEventListener.afterSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
    return GraduationRequestEventListener.beforeRemoveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

class GraduationRequestEventListener {

    static async beforeSaveAsync(event) {
        /**
         * @type {DataContext|*}
         */
        const context = event.model.context;
        if (event.state !== 4) {
            let request = event.model.convert(event.target);

            if (!request.student || !request.actionStatus || !request.graduationEvent) {
                // load request
                request = await context.model(event.model.name).where('id').equal(event.target.id).getTypedItem();
            }
            // check if actionStatus is changed
            let checkStudentStatus = true;
            if (event.state ===2) {
                const previousActionStatus = event.previous && event.previous.actionStatus;
                if (previousActionStatus === null) {
                    throw new Error('Previous status cannot be empty at this context.');
                } else {
                    // validate previous status
                    if (previousActionStatus.alternateName === request.actionStatus.alternateName) {
                        checkStudentStatus = false;
                    }
                }
            }
            if (checkStudentStatus) {
                // check student status
                const studentStatus = await context.model('Student').where('id').equal(request.student).select('studentStatus').expand('studentStatus').value();
                if (studentStatus) {
                    if (studentStatus.alternateName !== 'active' && studentStatus.alternateName !== 'declared') {
                        throw new GenericDataConflictError('ERR_STATUS',
                            context.__('Invalid student status. Student graduation request may execute upon an active or declared student only'), null,
                            'Student');
                    }
                }
            }
            //check if student department is the same with event organizer
            const department =  await context.model('Student').where('id').equal(request.student).select('department').flatten().value();
            const graduationEventId = _.isObject(request.graduationEvent)? request.graduationEvent.id : request.graduationEvent;
            /**
             * @type {Student|DataObject|*}
             */
            const student = context.model('Student').convert(request.student);
            const queryAvailableEvents =  await student.getAvailableGraduationEvents();
            const availableEvent =  await queryAvailableEvents.where('id').equal(graduationEventId).getItem();

            if (!availableEvent && event.state ===1) {
                throw new GenericDataConflictError('E_EVENT_NOT_FOUND',context.__('Event cannot be found'), null, 'GraduationRequestAction');
            }
            const graduationEvent = await context.model('GraduationEvent').where('id').equal(graduationEventId).getItem();
            if (!graduationEvent) {
                throw new GenericDataConflictError('E_EVENT_NOT_FOUND', context.__('Event cannot be found'), null, 'GraduationRequestAction');
            }
            if (department != graduationEvent.organizer) {
                throw new GenericDataConflictError('E_INVALID_ORGANIZER', context.__('Student cannot apply for another department graduation event'), null, 'GraduationRequestAction');
            }


        }
    }

    /**
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {
        // check previous and currentStatus
        try {
            const context = event.model.context;
            if (event.state === 2) {
                const request = event.model.convert(event.target);
                // get action status
                const actionStatus = await request.property('actionStatus').getItem();
                if (actionStatus) {
                    // if action status is other that active
                    if (actionStatus.alternateName !== ActionStatusType.CompletedActionStatus) {
                        // exit
                        return;
                    }
                    // get previous action status
                    const previousActionStatus = event.previous && event.previous.actionStatus;
                    if (previousActionStatus === null) {
                        throw new Error('Previous status cannot be empty at this context.');
                    }
                    // validate previous status
                    if (previousActionStatus.alternateName === 'ActiveActionStatus' && actionStatus.alternateName === 'CompletedActionStatus') {
                        // try to create declareAction
                        // check if student can be declared
                        const result = await request.validateRequest();
                        if (result && result.success === true) {
                            const item = await context.model('StudentDeclareAction')
                                .where('initiator').equal(event.target.id)
                                .and('actionStatus/alternateName').equal('ActiveActionStatus').getItem();
                            if (item == null) {
                                // convert current action
                                const newItem = await context.model('GraduationRequestAction')
                                    .where('id').equal(event.target.id)
                                    .select(
                                        'student as object',
                                        'agent as owner',
                                        'graduationEvent/graduationYear as declaredYear',
                                        'graduationEvent/graduationPeriod as declaredPeriod',
                                        'graduationEvent/startDate as graduationDate'
                                    )
                                    .getItem();
                                // validate that item exists
                                if (newItem == null) {
                                    throw new DataNotFoundError('The current action cannot be found or is inaccessible', null, 'GraduationRequestAction');
                                }
                                // validate owner
                                if (newItem.owner == null) {
                                    throw new GenericDataConflictError('E_EVENT_INITIATOR', 'The operation cannot be completed because initiator agent cannot be found or has not be set yet.', null, 'GraduationRequestAction');
                                }

                                Object.assign(newItem, {
                                    initiator: event.target.id, // set initiator (this action)
                                    actionStatus: { // set action status to active
                                        alternateName: 'ActiveActionStatus'
                                    },
                                    declaredDate: new Date()
                                });
                                // and finally save action
                                const action = await context.model('StudentDeclareAction').save(newItem);
                            }

                        } else {
                            // student rules not met for graduation
                            throw new GenericDataConflictError('E_GRADUATION', context.__('Graduation rules are not met or required documents are not approved'));
                        }
                    }
                }
            }
        } catch (err) {
            throw (err);
        }
    }

    static async beforeRemoveAsync(event) {
        // get context
        const context = event.model.context;
        const request = await context.model('GraduationRequestAction').where('id').equal(event.target.id).expand('actionStatus').getItem();
        // check if request status is not active or potential
        if (request.actionStatus && !(request.actionStatus.alternateName === ActionStatusType.ActiveActionStatus || request.actionStatus.alternateName === ActionStatusType.PotentialActionStatus)) {
            throw new DataError('E_REQUEST_STATUS', context.__('Invalid request status. Not active requests cannot be removed.'), null, 'GraduationRequestAction');
        }
        // check if graduation request is initiator of other action
        const hasGraduateActions = await context.model('StudentGraduateAction').where('initiator').equal(request.id)
            .silent().count();
        const hasDeclareActions = await context.model('StudentDeclareAction').where('initiator').equal(request.id)
            .silent().count();
        const hasDocumentActions = await context.model('RequestDocumentAction').where('initiator').equal(request.id)
            .silent().count();
        if (hasGraduateActions || hasDeclareActions || hasDocumentActions) {
            throw new DataError('E_ACTIONS_EXISTS', context.__('Cannot remove request. Graduation request is related to other actions.'), null, 'GraduationRequestAction');
        }
        const hasAttachments = await context.model('StudentRequestActionAttachments').where('object').equal(request.id)
            .silent().count();
        if (hasAttachments) {
            throw new DataError('E_ATTACHMENT_EXISTS', context.__('Cannot remove request. Graduation request is related to attachments.'), null, 'GraduationRequestAction');
        }
    }


    /**
     * @param {DataEventArgs} event
     */
    static async afterExecuteAsync(event) {

        // add available attachment types related with graduationEvent (only if one record is returned)
        let data = event['result'];
        // validate data
        if (data == null) {
            return;
        }
        // validate query and exit
        if (event.query.$group) {
            return;
        }
        if (!(Array.isArray(data) && data.length)) {
            return;
        }

        const context = event.model.context;
        // add attachmentTypes to request only if query returns 1 record
        if (data.length === 1) {
            // expand graduationEvent attachmentTypes
            // get graduation event
            const graduationEvent = _.isObject(data[0].graduationEvent) ? data[0].graduationEvent.id : data[0].graduationEvent;
            data[0].attachmentTypes = await context.model('GraduationAttachmentConfiguration').where('object/id').equal(graduationEvent).silent().getItems();
        }
    }
}
