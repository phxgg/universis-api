import StudyProgramSpecialty from '../models/study-program-specialty-model';
import {GenericDataConflictError} from "../errors";
import {DataModel} from "@themost/data";
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
    return beforeRemoveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * Get next specialty id while inserting a new specialty
 * @param {DataEventArgs} event
 */
export async function beforeSaveAsync(event) {
    // after insert
    if (event.state === 1) {
        // Get next specialty id while inserting a new specialty
        if (event.target.studyProgram) {
            const context = event.model.context;
            // get study program specialties
            const studyProgram = context.model('StudyProgram').convert((event.target.studyProgram)).getId();
            let lastSpecialtyIndex = await context.model('StudyProgramSpecialty')
                .where('studyProgram').equal(studyProgram)
                .select('max(specialty) as specialty').value();
            if (!lastSpecialtyIndex) {
                event.target.specialty = -1;
            } else {
                event.target.specialty = lastSpecialtyIndex === -1 ? 1 : lastSpecialtyIndex + 1;
            }

        } else {
            throw new GenericDataConflictError('E_STUDY_PROGRAM_MISSING', ('Study program is missing'), null, 'StudyProgramSpecialty');
        }
    }
}

/**
 * @param {DataEventArgs} event
 */
export async function beforeRemoveAsync(event) {
    const model = event.model;
    const getReferenceMappings = DataModel.prototype.getReferenceMappings;
    model.getReferenceMappings = async function () {
        const res = await getReferenceMappings.bind(this)();
        // remove readonly model StudentAvailableClass from mapping before delete
        const mappings = ['StudentAvailableClass', 'UpdateSpecialtyAction', 'UpdateProgramAction'];
        return res.filter((mapping) => {
            return mappings.indexOf(mapping.childModel) < 0;
        });
    };
}
