import {DataError, DataNotFoundError} from "@themost/common";
import {DataModel} from "@themost/data";
import _ from 'lodash';


function isEqual(previousReplacedCourse, targetReplacedCourse) {
    if (previousReplacedCourse == null && targetReplacedCourse == null) {
        return true;
    }
    let previous = previousReplacedCourse, target = targetReplacedCourse;
    // convert arguments from string to arrays
    if (previous && typeof previous === 'string') {
        previous = previous.split(',');
    }
    if (target && typeof target === 'string') {
        target = target.split(',');
    }
    // ensure all ids are trimmed and the arrays are sorted
    // note: use sort() to compare element wise,
    // even if the elements are not located at the same indexes
    if (Array.isArray(previous) && previous.length) {
        previous = (previous.map(id => id.trim())).sort();
    }
    if (Array.isArray(target) && target.length) {
        target = (target.map(id => id.trim())).sort();
    }
    // and compare via lodash
    return _.isEqual(previous, target);
}

class CourseListener {
    /**
     * @param {DataEventArgs} event
     */
    static async beforeSaveAsync(event) {

        const target = event.model.convert(event.target);
        const context = event.model.context;
        const previous =event.previous;

        // validate previous state
        if (event.state === 2 && previous == null) {
            throw new DataError('E_PREVIOUS', 'The previous state of the object cannot be determined', null, 'Course');
        }
        // get structure type
        const courseStructureType = event.target.courseStructureType;
        if (courseStructureType == null && event.state===2) {
           //load from course
            const structureType = await target.property('courseStructureType').getItem();
            target.courseStructureType = structureType;
        }
        //rule 1 : cannot change courseStructureType
        if (previous && (previous.courseStructureType != target.courseStructureType)) {
            throw new DataError('ERR_INVALID_DATA',
                context.__('Structure type cannot be changed')
                , null,
                'Course');
        }
        // rule 2: remove courseParts from update
        if (event.state===2 && target.courseParts && target.courseParts.length>0) {
            // remove target.courseParts
            delete event.target.courseParts;
        }
        // on insert
        if (event.state === 1 && target.courseParts && target.courseParts.length > 0) {
            // remove course parts and replace them with target.courses
            event.target.courses = event.target.courseParts;
            delete event.target.courseParts;
        }

        // get replacedCourse from target and previous states
        let replacedCourse = event.target.replacedCourse;
        // if replacedCourse comes as an array
        if (Array.isArray(replacedCourse)) {
            // convert it to appropriate format (string)
            replacedCourse = event.target.replacedCourse = replacedCourse.join(',');
        }
        const previousReplacedCourse = event.previous && event.previous.replacedCourse;
        // get replacedByCourse from target and previous states
        const replacedByCourse = event.target.replacedByCourse;
        const previousReplacedByCourse = event.previous && event.previous.replacedByCourse;

        // if replacedCourse has changed from the previous state
        if (replacedCourse && replacedCourse.length && !isEqual(previousReplacedCourse, replacedCourse)) {
            // validate course structure type
            // course parts cannot replace other courses
            if (courseStructureType === 8) {
                throw new DataError('ERR_INVALID_DATA',
                    context.__('Course parts cannot replace other courses') + `(${event.target.id})`
                    , null,
                    'Course');
            }
            // if the course has already been replaced by other courses, throw error
            if (replacedByCourse || previousReplacedByCourse) {
                throw new DataError('ERR_INVALID_DATA',
                    context.__('Replaced course cannot replace other courses') + `(${event.target.id})`
                    , null,
                    'Course');
            }
            // check if the course is disabled
            const isDisabled = event.target.hasOwnProperty('isEnabled') ? (event.target.isEnabled === 0 || event.target.isEnabled === false)
                            : (previous.isEnabled === 0 || previous.isEnabled === false);
            if (isDisabled) {
                // and throw error
                throw new DataError('ERR_INVALID_DATA',
                    context.__('Disabled courses cannot replace other courses') + `(${event.target.id})`
                    , null,
                    'Course');
            }
        }

        // if replacedByCourse has changed from the previous state
        if (replacedByCourse && !isEqual(previousReplacedByCourse, replacedByCourse)) {
            // validate course structure type
            // course parts cannot be replaced
            if (courseStructureType === 8) {
                throw new DataError('ERR_INVALID_DATA',
                    context.__('Course parts cannot be replaced') + `(${event.target.id})`
                    , null,
                    'Course');
            }
            // if the course is enabled
            const isEnabled = event.target.hasOwnProperty('isEnabled') ? (event.target.isEnabled !== 0 || event.target.isEnabled === true)
                : (previous.isEnabled !== 0 || previous.isEnabled === true);
            if (isEnabled) {
                 // disable it
                event.target.isEnabled = false;
            }
        }
        // on update, if the target object includes gradeScale
        if (event.state === 2 && target.gradeScale) {
            const targetGradeScale = typeof(target.gradeScale) === 'object'
                ? target.gradeScale.id : target.gradeScale;
            // note: grade scale is expandable (will be an object), but validate it in previous state also
            const previousGradeScale = typeof(previous.gradeScale) === 'object'
                ? previous.gradeScale.id : previous.gradeScale;
            // and it has a different value from the previous state
            if (previousGradeScale !== targetGradeScale) {
                // change can only be allowed if no grade has been calculated with the specified grade scale
                // first of all, try to find if some student grade trace exists for this course (bypass privileges)
                const scaleHasBeenUsedInTrace = await context.model('StudentGradeActionTrace')
                    .where('course').equal(target.id)
                    .and('examGrade').notEqual(null)
                    .select('id')
                    .silent()
                    .count();
                if (scaleHasBeenUsedInTrace) {
                    // if it has, throw error immediately
                    throw new DataError('E_GRADE_SCALE', 
                        context.__('The course grade scale cannot be changed as it has been used for at least one grade calculation'),
                        null,
                        'Course',
                        'gradeScale');
                }
                // as a second step, try to find if some student course exists for this course (bypass privileges)
                const scaleHasBeenUsedInStudentCourse = await context.model('StudentCourse')
                    .where('course').equal(target.id)
                    .and('grade').notEqual(null)
                    .select('id')
                    .silent()
                    .count();
                if (scaleHasBeenUsedInStudentCourse) {
                    // if it has, throw error immediately
                    throw new DataError('E_GRADE_SCALE', 
                        context.__('The course grade scale cannot be changed as it has been used for at least one grade calculation'),
                        null,
                        'Course',
                        'gradeScale');
                }
                // at this moment, change can be allowed for a simple course
                const structureType = previous.courseStructureType;
                // if the course is complex
                if (structureType === 4) {
                    // all of its parts must also pass the above validations
                    const targetCourse = await context.model('Course')
                        .where('id').equal(target.id)
                        .select('id')
                        .expand('courseParts')
                        .silent()
                        .getItem();
                    // validate course
                    if (targetCourse == null) {
                        throw new DataNotFoundError('The target course cannot be found');
                    }
                    // validate course parts
                    if (!(Array.isArray(targetCourse.courseParts) && targetCourse.courseParts.length)) {
                        throw new DataError('E_COURSE_PARTS', 'The parts of the complex course cannot be found.');
                    }
                    // map course parts id
                    const courseParts = targetCourse.courseParts.map(coursePart => coursePart.id);
                    // try to find if any of the parts have exists in a trace
                    const partScaleHasBeenUsedInTrace = await context.model('StudentGradeActionTrace')
                        .where('course').in(courseParts)
                        .and('examGrade').notEqual(null)
                        .select('id')
                        .silent()
                        .count();
                    if (partScaleHasBeenUsedInTrace) {
                        // if they do, throw error for complex course
                        throw new DataError('E_GRADE_SCALE', 
                            context.__('The course grade scale cannot be changed because at least one of its parts have been used in a grade calculation'),
                            null,
                            'Course',
                            'gradeScale');
                    }
                    // also validate that parts do not exist in any student course
                    const partScaleHasBeenUsedInStudentCourse = await context.model('StudentCourse')
                        .where('course').in(courseParts)
                        .and('grade').notEqual(null)
                        .select('id')
                        .silent()
                        .count();
                    if (partScaleHasBeenUsedInStudentCourse) {
                        // if they do, throw error for complex course
                        throw new DataError('E_GRADE_SCALE', 
                            context.__('The course grade scale cannot be changed because at least one of its parts have been used in a grade calculation'),
                            null,
                            'Course',
                            'gradeScale');
                    }
                } else if (structureType === 8) {
                    // if the course is a part of a complex one
                    // all of the siblings and the parent must pass the validations
                    const parentCourse = await context.model('Course')
                        .where('id').equal(previous.parentCourse)
                        .select('id')
                        .expand('courseParts')
                        .silent()
                        .getItem();
                    // validate parent course
                    if (parentCourse == null) {
                        throw new DataNotFoundError('The parent course of the specified course part cannot be found.');
                    }
                    // validate parent course parts
                    if (!(Array.isArray(parentCourse.courseParts) && parentCourse.courseParts.length)) {
                        throw new DataError('E_COURSE_PARTS', 'The parts of the parent course cannot be found.');
                    }
                    const siblingsAndParentCourses = parentCourse.courseParts.map(coursePart => coursePart.id);
                    // push parent id
                    siblingsAndParentCourses.push(parentCourse.id);
                    // but splice self
                    const findSelf = siblingsAndParentCourses.findIndex(courseId => courseId === target.id);
                    if (findSelf > -1) {
                        siblingsAndParentCourses.splice(findSelf, 1);
                    }
                    // and perform the validations
                    // first grade trace
                    const siblingsScaleHasBeenUsedInTrace = await context.model('StudentGradeActionTrace')
                        .where('course').in(siblingsAndParentCourses)
                        .and('examGrade').notEqual(null)
                        .select('id')
                        .silent()
                        .count();
                    if (siblingsScaleHasBeenUsedInTrace) {
                        throw new DataError('E_GRADE_SCALE', 
                            context.__('The course grade scale cannot be changed because at least one of its siblings or the parent course have been used in a grade calculation'),
                            null,
                            'Course',
                            'gradeScale');
                    }
                    // and then student course
                    const siblingsScaleHasBeenUsedInStudentCourse = await context.model('StudentCourse')
                        .where('course').in(siblingsAndParentCourses)
                        .and('grade').notEqual(null)
                        .select('id')
                        .silent()
                        .count();
                    if (siblingsScaleHasBeenUsedInStudentCourse) {
                        throw new DataError('E_GRADE_SCALE', 
                            context.__('The course grade scale cannot be changed because at least one of its siblings or the parent course have been used in a grade calculation'),
                            null,
                            'Course',
                            'gradeScale');
                    }
                }
            }
        }
    }

    /**
     * @async
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {
        const context = event.model.context;
        if (event.state===1) {
            if (event.target.courses && event.target.courses.length > 0 && event.target.courseStructureType === 4) {
                // check if coursePart exists and throw error
                const courseIds = event.target.courses.map(coursePart => {
                    return coursePart.id;
                });
                if (courseIds) {
                    const courses = await context.model('Course').where('id').in(courseIds).getItems();
                    if (courses.length > 0) {
                        throw new DataError('ERR_INVALID_DATA',
                            context.__('Course is not available for coursePart')
                            , null,
                            'Course');
                    }
                }
                let i = 0;
                event.target.courses.forEach(course => {
                    i += 1;
                    course.id = course.id ||  `${event.target.id}` + i;
                    course.courseStructureType = 8;
                    course.department = event.target.department;
                    course.gradeScale = event.target.gradeScale;
                    course.parentCourse = event.target.id;
                    course.instructor = course.instructor || event.target.instructor;
                    course.$state = 1;
                });
                await context.model('Course').save(event.target.courses);
            }
        }
        if (event.state === 2) {
            // get replacedCourse in target and previous states
            const replacedCourses = event.target.replacedCourse;
            const previousReplacedCourses = event.previous.replacedCourse;
            if (replacedCourses) {
                if (!previousReplacedCourses || !isEqual(previousReplacedCourses, replacedCourses)) {
                    // get relevant courses
                    const replacedCourseIDs = replacedCourses.split(',').map(id => id.trim());
                    let courses = await context.model('Course').where('id').in(replacedCourseIDs).getItems();
                    // hold only courses that need to be updated
                    let updateCourses = [];
                    let removedReplacementIDs = [];
                    courses.forEach(course => {
                        // if replacedByCourse has not already been set
                        if (course.replacedByCourse != event.target.id) {
                            // set it
                            course.replacedByCourse = event.target.id;
                            // and push course for update
                            updateCourses.push(course);
                        }
                    });
                    // if there is a previous state
                    if (previousReplacedCourses) {
                        // try to find if any course has been removed in the target state
                        const previousReplacedCourseIDs = previousReplacedCourses.split(',').map(id => id.trim());
                        removedReplacementIDs = previousReplacedCourseIDs.filter(courseId => {
                            return replacedCourseIDs.indexOf(courseId) < 0;
                        });
                        // and for any course that has been removed as a replacement
                        if (removedReplacementIDs && removedReplacementIDs.length) {
                            // fetch it 
                            const removedReplacements = await context.model('Course').where('id').in(removedReplacementIDs).getItems();
                            // set the replacedByCourse attribute to null (if it is not already)
                            removedReplacements.forEach(course => {
                                if (course.replacedByCourse) {
                                    course.replacedByCourse = null;
                                    // and push for update
                                    updateCourses.push(course);
                                }
                            });
                        }
                    }
                    // and update courses, if any
                    if (updateCourses && updateCourses.length) {
                        await context.model('Course').save(updateCourses);
                    }
                }
            } else {
                // if replacedCourse is null, but it is present in the previous state
                // important note: replacedCourse must exist in the target object (and be null)
                if (previousReplacedCourses && event.target.hasOwnProperty('replacedCourse')) {
                    const updateCourses = [];
                    // fetch all previously replaced courses
                    const previousReplacedCourseIDs = previousReplacedCourses.split(',').map(id => id.trim());
                    const courses = await context.model('Course').where('id').in(previousReplacedCourseIDs).getItems();
                    courses.forEach(course => {
                        if (course.replacedByCourse) {
                            // reset replacedByCourse field
                            course.replacedByCourse = null;
                            // and push for update
                            updateCourses.push(course);
                        }
                    });
                    if (updateCourses && updateCourses.length) {
                        await context.model('Course').save(updateCourses);
                    }
                }
            }
            // get replacedByCourse in previous and target states
            const replacedByCourse = event.target.replacedByCourse;
            const previousReplacedByCourse = event.previous.replacedByCourse;
            if (replacedByCourse) {
                if (!previousReplacedByCourse || !isEqual(previousReplacedByCourse, replacedByCourse)) {
                    // get relevant course
                    const course = await context.model('Course').where('id').equal(replacedByCourse).getItem();
                    if (course.replacedCourse) {
                        // try to find if current course belongs to the replacedCourse array of the target one
                        const replacedCourseIDs = course.replacedCourse.split(',').map(id => id.trim());
                        const findIndex = replacedCourseIDs.findIndex(courseId => courseId == event.target.id.toString().trim());
                        if (findIndex < 0) {
                            // if not, push to array and convert back to string
                            replacedCourseIDs.push(event.target.id.toString());
                            course.replacedCourse = replacedCourseIDs.join(',');
                            // and save
                            await context.model('Course').save(course);
                        }
                    } else {
                        // update replacedCourse field of the course that replaces the current
                        course.replacedCourse = event.target.id.toString();
                        // and save
                        await context.model('Course').save(course);
                    }
                }
            } else {
                // if replacedByCourse is null, but it is present in the previous state
                // important note: replacedByCourse must exist in the target object (and be null)
                if (previousReplacedByCourse && event.target.hasOwnProperty('replacedByCourse')) {
                    // get relevant course
                    const course = await context.model('Course').where('id').equal(previousReplacedByCourse).getItem();
                    if (course.replacedCourse) {
                        // try to find it in replacedCourse array of linked course
                        const replacedCourseIDs = course.replacedCourse.split(',').map(id => id.trim());
                        const findIndex = replacedCourseIDs.findIndex(courseId => courseId == event.target.id.toString().trim());
                        // if it exists
                        if (findIndex > -1) {
                            // remove course from replacedCourse field of the course that replaced it
                            replacedCourseIDs.splice(findIndex, 1);
                            // and update the replacedCourse field of linked course
                            if (replacedCourseIDs.length === 0) {
                                course.replacedCourse = null;
                            } else {
                                course.replacedCourse = replacedCourseIDs.join(',');
                            }
                            // and save
                            await context.model('Course').save(course);
                        }
                    }
                }
            }
            if (event.target.hasOwnProperty('isEnabled') && Math.abs(event.previous.isEnabled) !== Math.abs(event.target.isEnabled) && event.previous.courseStructureType === 4) {
                let courseParts = await context.model('Course')
                    .where('parentCourse').equal(event.target.id)
                    .select('id', 'courseStructureType')
                    .silent().getAllItems();
                if (courseParts && courseParts.length) {
                    courseParts = courseParts.map(coursePart => {
                        coursePart.isEnabled = event.target.isEnabled;
                        return coursePart;
                    });
                    await context.model('Course').save(courseParts);
                }
            }
        }
    }

    /**
     * @param {DataEventArgs} event
     */
    static async beforeRemoveAsync(event) {
        const context = event.model.context;
        const target = event.target;
        const model = event.model;
        const getReferenceMappings = DataModel.prototype.getReferenceMappings;
        model.getReferenceMappings = async function () {
            const res = await getReferenceMappings.bind(this)();
            // remove readonly model CourseLocale and rules from mapping before delete
            const mappings = ['CourseExamStudentGrade', 'StudentAvailableClass',
                'CourseLocale', 'CourseAreaRuleEx',
                'CourseCategoryRuleEx', 'CourseSectorRuleEx',
                'CourseRuleEx', 'CourseTypeRuleEx',
                'MeanGradeRuleEx', 'ProgramGroupRuleEx', 'RegisteredCourseRuleEx', 'YearMeanGradeRuleEx'];
            return res.filter((mapping) => {
                return mappings.indexOf(mapping.childModel) < 0;
            });
        };
        // if course is complex try to remove course parts
        const course = await context.model('Course')
            .where('id').equal(event.target.id)
            .and('courseStructureType').equal(4)
            .select('id', 'courseStructureType')
            .expand('courseParts')
            .silent()
            .getItem();
        if (course && Array.isArray(course.courseParts) && course.courseParts.length) {
            await context.model('Course').remove(course.courseParts.map(x=>{
                return {id: x.id};
            }));
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return CourseListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return CourseListener.afterSaveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
    return CourseListener.beforeRemoveAsync(event).then(() => {
        return callback();
    }).catch (err => {
        return callback(err);
    });
}
