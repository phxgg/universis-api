import { ApplicationServiceAsListener } from "../services/application-service-as-listener";
import { getMailer } from '@themost/mailer';
import { Args, TraceUtils } from '@themost/common';
import moment from 'moment';

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return callback();
}

/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    const context = event.model.context;
    if (event.state !== 1) {
        return;
    }
    const mailTemplate = await context.model('MailConfiguration').where('target').equal('EmailAfterRegisterAction').silent().getItem();
    if (mailTemplate == null) {
        return;
    }
    const student = await context.model('Student').find(event.target.object).expand('person').silent().getItem();
    Args.notNull(student, 'Student');
    Args.notNull(student.person, 'Recipient');
    //get target object
    const target = await event.model.where('id').equal(event.target.id).getItem();

    const mailer = getMailer(context);

    const data = {
        'student': student,
        'model': target
    };

    // check if student's email exists
    if (student.person.email == null || (student.person.email && typeof student.person.email !== 'string')) {
        TraceUtils.warn(`StudentRegisterAction: Cannot send email for ${student.person.familyName} ${student.person.givenName}. Student [${student.id}] email does not exist.`);
        return;
    }

    return await new Promise((resolve, reject) => {
        mailer.to(student.person.email)
            .subject(mailTemplate.subject)
            .template(mailTemplate.template)
            .send(Object.assign(data, {
                html: {
                    moment: moment,
                    context: context
                }
            }), (err) => {
                if (err) {
                    try {
                        TraceUtils.error('An error occurred while trying to send an email notification after student register action.');
                        TraceUtils.error(`student=${student.person.email}, template=${mailTemplate.template}`)
                        TraceUtils.error(err);
                    } catch (err1) {
                        // do nothing
                    }
                    return resolve();
                }
                return resolve();
            });
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {

    afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        TraceUtils.error('SendEmailAfterRegisterActionListener.afterSave()');
        TraceUtils.error(err);
        return callback(err);
    });
}

export class SendEmailAfterRegisterAction extends ApplicationServiceAsListener {
    constructor(app) {
        super(app);
        // install this listener as member of event listeners of StudentRegisterAction model
        this.install('StudentRegisterAction', __filename);

        // import default mail configuration
        (async function () {
            const context = app.createContext();
            const mailConfiguration = await context.model('MailConfiguration').where('target').equal('EmailAfterRegisterAction').silent().getItem();
            if (mailConfiguration == null) {
                await context.model('MailConfiguration').silent().save({
                    "target": "EmailAfterRegisterAction",
                    "status": 1,
                    "previouStatus": 1,
                    "subject": context.__("Student semester register action") ,
                    "template": "mail-after-semester-register-action",
                    "state": 1
                });
            }
            context.finalize();
        })().then(() => {
            //
        }).catch(err => {
            TraceUtils.error(err);
        });
    }
}
