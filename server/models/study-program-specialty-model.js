import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';
import {promisify} from "es6-promisify";
import {DataObjectState, DataPermissionEventListener} from "@themost/data";
import {DataNotFoundError} from "@themost/common/errors";
const Rule = require('./rule-model');

/**
 * @class
 
 * @property {string} id
 * @property {StudyProgram|any} studyProgram
 * @property {number} specialty
 * @property {string} name
 * @property {string} abbreviation
 */
@EdmMapping.entityType('StudyProgramSpecialty')
class StudyProgramSpecialty extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    async getStudyProgram() {
        const item = this.getModel().where('id').equal(this.getId())
            .select('studyProgram')
            .expand('studyProgram')
            .value();
        this.context.model('StudyProgram').convert(item);
    }

    /**
     * Returns a collection of rules which are going to be check while validating
     * that a student is eligible to get a program specialization
     * @returns {Promise<any[]>}
     */
    @EdmMapping.func('SpecialtyRules', EdmType.CollectionOf('Rule'))
    async getSpecialtyRules() {
        /**
         * @type {{studyProgram: *, specialty: *} | null}
         */
        const snapshot = await this.getModel().where('id').equal(this.getId())
            .select('studyProgram', 'specialty')
            .getItem();
        if (snapshot == null) {
            throw new DataNotFoundError('Current specialization cannot be found or is inaccessible', null, 'StudyProgramSpecialty');
        }
        // return rules of type ProgramCourseRegistrationRule
        const items = await Rule.expand(this.context, snapshot.studyProgram.toString(),
            'Program', 'SpecialtyRule');
        // todo: fix this backward compatibility issue
        // filter rules by specialty index
        return items.filter((item) => {
            return item.programSpecialty == snapshot.specialty;
        });
    }

    /**
     * Updates program specialization rules
     * @returns {Promise<any[]>}
     */
    @EdmMapping.param('items', EdmType.CollectionOf('Rule'), false, true)
    @EdmMapping.action('SpecialtyRules', EdmType.CollectionOf('Rule'))
    async setSpecialtyRules(items) {
        const finalItems = [];
        /**
         * get item snapshot
         * @type {{studyProgram: *, specialty: *} | null}
         */
        const snapshot = await this.getModel().where('id').equal(this.getId())
            .select('studyProgram', 'specialty')
            .getItem();
        if (snapshot == null) {
            throw new DataNotFoundError('Current specialization cannot be found or is inaccessible', null, 'StudyProgramSpecialty');
        }
        for(let item of items) {
            // convert item
            const converted = await Rule.flatten(this.context, item, `${item.refersTo}RuleEx`);
            // set target id
            converted.target = snapshot.studyProgram.toString();
            // set target type
            converted.targetType = 'Program';
            // set additional type
            converted.additionalType = 'SpecialtyRule';
            // set specialty
            converted.programSpecialty = snapshot.specialty;
            // add item
            finalItems.push(converted);
        }
        // validate permissions
        const validateAsync = promisify(DataPermissionEventListener.prototype.validate);
        await validateAsync({
            model: this.getModel(), // set current model
            state: DataObjectState.Update, // set state to update
            target: this // this object is the target object
        });
        // save items
        await this.context.model(Rule).silent().save(finalItems);
        // and return collection of rules
        return this.getSpecialtyRules();
    }

    /**
     * Returns a collection of rules which are going to be check while validating that a student which follows this specialization is eligible for graduation
     * @returns {Promise<any[]>}
     */
    @EdmMapping.func('GraduationRules', EdmType.CollectionOf('Rule'))
    async getGraduationRules() {
        /**
         * @type {{studyProgram: *, specialty: *} | null}
         */
        const snapshot = await this.getModel().where('id').equal(this.getId())
            .select('studyProgram', 'specialty')
            .getItem();
        if (snapshot == null) {
            throw new DataNotFoundError('Current specialization cannot be found or is inaccessible', null, 'StudyProgramSpecialty');
        }
        // return rules of type ProgramCourseRegistrationRule
        const items = await Rule.expand(this.context, snapshot.studyProgram.toString(),
            'Program', 'GraduateRule');
        // todo: fix this backward compatibility issue
        // filter rules by specialty index
        return items.filter((item) => {
            return item.programSpecialty == snapshot.specialty;
        });
    }

    /**
     * Updates program specialization graduation rules
     * @returns {Promise<any[]>}
     */
    @EdmMapping.param('items', EdmType.CollectionOf('Rule'), false, true)
    @EdmMapping.action('GraduationRules', EdmType.CollectionOf('Rule'))
    async setGraduationRules(items) {
        const finalItems = [];
        /**
         * get item snapshot
         * @type {{studyProgram: *, specialty: *} | null}
         */
        const snapshot = await this.getModel().where('id').equal(this.getId())
            .select('studyProgram', 'specialty')
            .getItem();
        if (snapshot == null) {
            throw new DataNotFoundError('Current specialization cannot be found or is inaccessible', null, 'StudyProgramSpecialty');
        }
        for(let item of items) {
            // convert item
            const converted = await Rule.flatten(this.context, item, `${item.refersTo}RuleEx`);
            // set target id
            converted.target = snapshot.studyProgram;
            // set target type
            converted.targetType = 'Program';
            // set additional type
            converted.additionalType = 'GraduateRule';
            // set specialty
            converted.programSpecialty = snapshot.specialty;
            // add item
            finalItems.push(converted);
        }
        // validate permissions
        const validateAsync = promisify(DataPermissionEventListener.prototype.validate);
        await validateAsync({
            model: this.getModel(), // set current model
            state: DataObjectState.Update, // set state to update
            target: this // this object is the target object
        });
        // save items
        await this.context.model(Rule).silent().save(finalItems);
        // and return collection of rules
        return this.getGraduationRules();
    }

}
module.exports = StudyProgramSpecialty;
