import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';
/**
 * @class
 */
declare class Alert extends DataObject {

     
     public id: number; 
     
     /**
      * @description The alert title.
      */
     public title?: string; 
     
     /**
      * @description The message body of the alert.
      */
     public body?: string; 
     
     /**
      * @description The message recipient (0 everyone).
      */
     public recipient?: number; 
     
     /**
      * @description The relevant model
      */
     public relModel?: string; 
     
     /**
      * @description The relevant item.
      */
     public relItem?: number; 
     
     /**
      * @description The date and time when the item was created.
      */
     public dateCreated?: Date; 
     
     /**
      * @description Η ημερομηνία τροποποίησης του στοιχείου.
      */
     public dateModified?: Date; 
     
     /**
      * @description Created by user.
      */
     public createdBy?: number; 
     
     /**
      * @description Modified by user.
      */
     public modifiedBy?: number; 

}

export = Alert;