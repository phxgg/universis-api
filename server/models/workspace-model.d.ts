import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';
/**
 * @class
 */
declare class Workspace extends DataObject {

     
     /**
      * @description Μοναδικός κωδικός
      */
     public id: string; 
     
     /**
      * @description Status
      */
     public databaseStatus?: string; 
     
     /**
      * @description Ημερομηνία τελευταίας ενημέρωσης
      */
     public dateLastAttached?: Date; 
     
     /**
      * @description Ημερομηνία τελευταίας τροποποίησης
      */
     public dateModified: Date; 

}

export = Workspace;