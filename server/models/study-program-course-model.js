import {EdmMapping, EdmType, DataObject, DataPermissionEventListener, DataObjectState} from '@themost/data';
import {promisify} from "es6-promisify";
const Rule = require('./rule-model');
/**
 * @class
 
 * @property {number} id
 * @property {Course|any} course
 * @property {Course|any} studyProgram
 * @property {ProgramGroup|any} programGroup
 * @property {number} programGroupFactor
 * @property {number} sortIndex
 * @property {string} identifier
 * @property {Date} dateModified
 */
@EdmMapping.entityType('StudyProgramCourse')
class StudyProgramCourse extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * Returns a collection or rules which are going to be validated during course registration
     * @returns {Promise<any[]>}
     */
    @EdmMapping.func('ProgramCourseRegistrationRules', EdmType.CollectionOf('Rule'))
    async getProgramCourseRegistrationRules() {
        // return rules of type ProgramCourseRegistrationRule
        return Rule.expand(this.context, this.getId(),
            'ProgramCourse', 'ProgramCourseRegistrationRule');
    }

    /**
     * Updates program course rules
     * @returns {Promise<any[]>}
     */
    @EdmMapping.param('items', EdmType.CollectionOf('Rule'), false, true)
    @EdmMapping.action('ProgramCourseRegistrationRules', EdmType.CollectionOf('Rule'))
    async setProgramCourseRegistrationRules(items) {
        const finalItems = [];
        for(let item of items) {
            // convert item
            const converted = await Rule.flatten(this.context, item, `${item.refersTo}RuleEx`);
            // set target id
            converted.target = this.getId().toString();
            // set target type
            converted.targetType = 'ProgramCourse';
            // set additional type
            converted.additionalType = 'ProgramCourseRegistrationRule';
            // add item
            finalItems.push(converted);
        }
        // validate permissions
        const validateAsync = promisify(DataPermissionEventListener.prototype.validate);
        await validateAsync({
            model: this.getModel(), // set current model
            state: DataObjectState.Update, // set state to update
            target: this // this object is the target object
        });
        // save items
        await this.context.model(Rule).silent().save(finalItems);
        // and return new collection
        return this.getProgramCourseRegistrationRules();
    }
}
module.exports = StudyProgramCourse;
