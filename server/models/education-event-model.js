import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';
import { Event } from '@universis/events';
/**
 * @class
 
 * @property {string} assesses
 * @property {EducationLevel|any} educationalLevel
 * @property {string} teaches
 * @property {number} presenceType
 * @property {Array<EducationEventAttendance|any>} attendanceList
 * @property {number} id
 * @augments {DataObject}
 */
@EdmMapping.entityType('EducationEvent')
class EducationEvent extends Event {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = EducationEvent;
