import {EdmMapping,EdmType} from '@themost/data/odata';
import RequestAction = require('./request-action-model');

/**
 * @class
 */
declare class RequestMessageAction extends RequestAction {

     
     /**
      * @description Μοναδικός κωδικός
      */
     public id: number; 

}

export = RequestMessageAction;