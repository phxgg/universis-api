import {assert} from 'chai';
import app from '../app';
import {ExpressDataApplication} from '@themost/express';
// eslint-disable-next-line no-unused-vars
import {getMailer,MailHelper} from '@themost/mailer';
import TEST_STUDENT_REGISTRATION from './test-mailer-student-registration';
import path from 'path';
import moment from 'moment';
import {MailHelperErrorCatcher} from "../services/mail-helper-error-catcher";
describe('Mailer', () => {

    /**
     * @type ExpressDataContext
     */
    let context;
    /**
     * @type MailHelper|*
     */
    let mailer;
    before(done => {
        // eslint-disable-next-line no-console
        console.log('INFO', 'IMPORTANT NOTE', 'Please install and start an SMTP development server from https://github.com/djfarrelly/MailDev to run this test');
        // set application configuration mail settings to local development mail server
        app.get(ExpressDataApplication.name).getConfiguration().setSourceAt('settings/mail', {
            port: 1025,
            ignoreTLS: true
        });
        context = app.get(ExpressDataApplication.name).createContext();
        context.user = {
            name: 'user1@example.com'
        };
        mailer = getMailer(context);
       return done();
    });

    it('should send an html message', async () => {
        context.getApplication().useService(MailHelperErrorCatcher);
        assert.isObject(mailer);
        await new Promise((resolve, reject) => {
            mailer.from('test@example.com')
                .to('user1@example.com')
                .subject('Test Message')
                .body('<h1>Hello World!</h1>')
                .send(null, (err) => {
                    if (err) {
                        return reject(err);
                    }
                    return resolve();
                });
        });
    });

});
