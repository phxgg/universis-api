import app from '../server/app';
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {TestUtils} from '../server/utils';
import { SendSmsAfterCreateCandidateUser } from '@universis/candidates';
// eslint-disable-next-line no-unused-vars
const executeInTransaction = TestUtils.executeInTransaction;
describe('SMS', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        context = app1.createContext();
        return done();
    });
    afterAll( done => {
        if (context) {
            return context.finalize( ()=> done() );
        }
        return done();
    });
    it('should create candidate and send sms', async ()=> {
        await executeInTransaction(context, async ()=> {

            context.user = {
                name: 'registrar1@example.com'
            };

            context.getApplication().useService(SendSmsAfterCreateCandidateUser);

            const department = await context.model('LocalDepartment').asQueryable().getItem();
            const studyProgram = await context.model('StudyProgram')
                .where('department').equal(department)
                .and('isActive').equal(true)
                .orderByDescending('id')
                .getItem();

            // create user
            const result = await context.model('CandidateStudent').save({
                "studyProgram": studyProgram,
                "inscriptionNumber": 340004000,
                "person": {
                    "gender": 1,
                    "familyName": "Rees",
                    "givenName": "Alexis",
                    "fatherName": "George",
                    "motherName": "Μaria",
                    "email": "candidate100@example.com",
                    "mobilePhone": "+306900000000"
                },
                "semester": 1,
                "inscriptionSemester": 1,
                "inscriptionYear": 2018,
                "inscriptionPeriod": 1,
                "inscriptionMode": 1,
                "specialtyId": -1,
                "department": department,
                "studentStatus": {
                    "alternateName": "candidate"
                }
            });
            let candidateStudent = await context.model('CandidateStudent')
                .where('id').equal(result.id).getTypedItem();
            expect(candidateStudent).toBeTruthy();
            candidateStudent = await candidateStudent.createUser();
            // get candidate again
            candidateStudent = await context.model('CandidateStudent')
                .where('id').equal(result.id).expand('user').getTypedItem();
            expect(candidateStudent).toBeTruthy();
            expect(candidateStudent.user).toBeTruthy();
            const service = context.getApplication().getService(function OAuth2ClientService() {});
            const token = await service.authorize(service.settings.adminAccount);
            const user = await service.getUser(candidateStudent.user.name, {
                access_token: token.access_token
            });
            expect(user).toBeTruthy();
            const activationCode = await context.model('CandidateUser')
            .where('id').equal(candidateStudent.user).select('activationCode')
            .silent().value();
            expect(activationCode).toBeTruthy();
            // try to login
            let newToken = await service.authorize({
                client_id: service.settings.client_id,
                client_secret: service.settings.client_secret,
                username: candidateStudent.user.name,
                password: activationCode,
                grant_type: 'password',
                scope: 'students'
            });
            expect(newToken).toBeTruthy();
            await service.deleteUser(user, {
                access_token: token.access_token
            });
        });
    });

});
