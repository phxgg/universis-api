// eslint-disable-next-line no-unused-vars
import StudyProgram from '../../server/models/study-program-model';
import {DEPARTMENT} from './Department';

/**
 * @type {StudyProgram}
 */
const STUDY_PROGRAM = {
    name: 'Master in Law',
    department: DEPARTMENT,
    degreeDescription: 'Master in Law',
    abbreviation: 'ML',
    decimalDigits: 2,
    hasFees: false,
    isActive: true,
    printName: 'Master in Law',
    semesters: 8,
    studyLevel: 1,
    gradeScale: null,
    specialtySelectionSemester: 0
};

export {
    STUDY_PROGRAM
};
